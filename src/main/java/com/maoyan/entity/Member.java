package com.maoyan.entity;

import java.util.Date;

public class Member {
    private Integer id;
    private String phonenum;
    private String password;
    private String membernickname;
    private String email;
    private String memberimg;
    private Integer sex;
    private Date birth;
    private String creator;
    private Date createtime;
    private Date operatetime;
    private String operator;
    private Integer deleteflag;

    private Integer membertotalintegral;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPhonenum() {
        return phonenum;
    }

    public void setPhonenum(String phonenum) {
        this.phonenum = phonenum;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMembernickname() {
        return membernickname;
    }

    public void setMembernickname(String membernickname) {
        this.membernickname = membernickname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMemberimg() {
        return memberimg;
    }

    public void setMemberimg(String memberimg) {
        this.memberimg = memberimg;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Date getOperatetime() {
        return operatetime;
    }

    public void setOperatetime(Date operatetime) {
        this.operatetime = operatetime;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public Integer getDeleteflag() {
        return deleteflag;
    }

    public void setDeleteflag(Integer deleteflag) {
        this.deleteflag = deleteflag;
    }

    public Integer getMembertotalintegral() {
        return membertotalintegral;
    }

    public void setMembertotalintegral(Integer membertotalintegral) {
        this.membertotalintegral = membertotalintegral;
    }

    @Override
    public String toString() {
        return "Member{" +
                "id=" + id +
                ", phonenum='" + phonenum + '\'' +
                ", password='" + password + '\'' +
                ", membernickname='" + membernickname + '\'' +
                ", email='" + email + '\'' +
                ", memberimg='" + memberimg + '\'' +
                ", sex=" + sex +
                ", birth=" + birth +
                ", creator='" + creator + '\'' +
                ", createtime=" + createtime +
                ", operatetime=" + operatetime +
                ", operator='" + operator + '\'' +
                ", deleteflag=" + deleteflag +
                ", membertotalintegral=" + membertotalintegral +
                '}';
    }
}
