package com.maoyan.entity;

import java.util.Date;

public class MemberIntegral {

    private Integer id;
    private Integer memberid;
    private Integer memberintegral;
    private String creator;
    private Date createtime;
    private Date operatetime;
    private String operator;
    private Integer deleteflag;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMemberid() {
        return memberid;
    }

    public void setMemberid(Integer memberid) {
        this.memberid = memberid;
    }

    public Integer getMemberintegral() {
        return memberintegral;
    }

    public void setMemberintegral(Integer memberintegral) {
        this.memberintegral = memberintegral;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Date getOperatetime() {
        return operatetime;
    }

    public void setOperatetime(Date operatetime) {
        this.operatetime = operatetime;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public Integer getDeleteflag() {
        return deleteflag;
    }

    public void setDeleteflag(Integer deleteflag) {
        this.deleteflag = deleteflag;
    }

    @Override
    public String toString() {
        return "MemberIntegral{" +
                "id=" + id +
                ", memberid=" + memberid +
                ", memberintegral=" + memberintegral +
                ", creator='" + creator + '\'' +
                ", createtime=" + createtime +
                ", operatetime=" + operatetime +
                ", operator='" + operator + '\'' +
                ", deleteflag=" + deleteflag +
                '}';
    }
}
