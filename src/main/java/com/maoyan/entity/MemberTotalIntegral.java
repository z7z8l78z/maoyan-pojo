package com.maoyan.entity;

import java.util.Date;

public class MemberTotalIntegral {

    private Integer id;
    private Integer memberid;
    private Integer membertotalintegral;
    private String creator;
    private Date createtime;
    private Date operatetime;
    private String operator;
    private Integer deleteflag;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMemberid() {
        return memberid;
    }

    public void setMemberid(Integer memberid) {
        this.memberid = memberid;
    }

    public Integer getMembertotalintegral() {
        return membertotalintegral;
    }

    public void setMembertotalintegral(Integer membertotalintegral) {
        this.membertotalintegral = membertotalintegral;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Date getOperatetime() {
        return operatetime;
    }

    public void setOperatetime(Date operatetime) {
        this.operatetime = operatetime;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public Integer getDeleteflag() {
        return deleteflag;
    }

    public void setDeleteflag(Integer deleteflag) {
        this.deleteflag = deleteflag;
    }

    @Override
    public String toString() {
        return "MemberTotalIntegral{" +
                "id=" + id +
                ", memberid=" + memberid +
                ", membertotalintegral=" + membertotalintegral +
                ", creator='" + creator + '\'' +
                ", createtime=" + createtime +
                ", operatetime=" + operatetime +
                ", operator='" + operator + '\'' +
                ", deleteflag=" + deleteflag +
                '}';
    }
}
